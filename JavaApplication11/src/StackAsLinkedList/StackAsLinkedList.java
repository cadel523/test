/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package StackAsLinkedList;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * 
  Title:           Programming Assignment: Linked List as a Stack
  Semester:        COP3337 – Fall 2017
  @author          5793866
  Instructor:      C. Charters
  
   Due Date:       11/26/2017

 * In this program we make use of our modified linked list which works as a stack
 * in order to push data read from a file into a stack and then print the data 
 * backwards.
 */
public class StackAsLinkedList 
{

    LinkedListStack stack = new LinkedListStack();
    /**
     * The main method creates an instance of its own class and calls the 
     * methods we created.
     * @param args 
     */
    public static void main(String[] args)
    {
        StackAsLinkedList fun = new StackAsLinkedList();
        fun.readFileLoadStack();
      //  fun.popStackPrintMsg();
      
        
    }
    /**
     * We read a single line of the file into a string, we then parse the file
     * by the spaces between every word and put every word into an array. Then 
     * using a for loop we push every word of that array into a stack. This push
     * method takes an object as an input so we static cast the string into an 
     * Object. After we are doing pushing every word into a stack we call our 
     * popStackPrintMsg method which prints our sentence backwards. 
     */
    public void readFileLoadStack()
    {
        File file = new File("thing.txt");
        try
        {
            Scanner readFile = new Scanner(file);
            while(readFile.hasNext())
            {
                String word = readFile.nextLine();
                String[] hold = word.split(" ");
                for(int i =0; i < hold.length; i++)
                {
                   // System.out.println(hold[i]);
                    stack.push((Object)hold[i]);
                }
                popStackPrintMsg();
            }
        }
        catch (FileNotFoundException e)
        {
            System.out.println("File not found bruh...");
        }
        
        
    }
    /**
     * When printing a message while the stack is not empty pop the last element 
     * of the stack and add it to a string. when the loop is over meaning that 
     * the stack is empty or rather it points to null. Print the string and 
     * instantiate the string to a new block of memory. 
     */
    public void popStackPrintMsg()
    {
        String song = "";
        while(!stack.empty())
        {
            song +=stack.pop() +" ";
        }
        System.out.println(song);
        song = new String();
    }
}
